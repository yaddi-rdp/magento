<?php 
namespace Reddot\RapiDua\Controller\RapiDua; 
 
use Magento\Framework\Controller\ResultFactory;
use Reddot\RapiDua\Helper\LogHelper;
use Reddot\RapiDua\Helper\Merchant;

class Index extends \Magento\Framework\App\Action\Action
{ 

    protected $config;
    protected $orderRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Reddot\RapiDua\Helper\Config $config,
        \Magento\Sales\Api\Data\OrderInterface $orderRepository
    ) {
        $this->config = $config;
        $this->orderRepository = $orderRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $transaction_id = $_GET['transaction_id'];

        $url = 'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_redirection';

        if ($this->config->getGeneralConfig('environment') == 'production') {
            $url = 'https://secure.reddotpayment.com/service/Merchant_processor/query_redirection';
        }

        $request_params = (new Merchant())->makeRequestGetOrderInfo($transaction_id, $this->config);
        $response = (new Merchant())->callMerchantApi('POST', $request_params, $url);

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ((int)$response->response_code == 0 || $response->response_msg == 'successful'){
            $resultRedirect->setPath('checkout/onepage/success');
        }else{
            $resultRedirect->setPath('checkout/onepage/failure');
        }

        return $resultRedirect;
    }
}