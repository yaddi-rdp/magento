<?php 
namespace Reddot\RapiDua\Controller\RapiDua; 
 
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;
use Reddot\RapiDua\Helper\LogHelper;

class RapiBackAccountDetail extends \Magento\Framework\App\Action\Action
{ 
    protected $orderRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Api\Data\OrderInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $storeManager = $this->_objectManager->create('Magento\Store\Model\StoreManagerInterface');

        $order = $this->orderRepository->loadByIncrementId($_GET['order_id']);
        $order->setStatus(Order::STATE_CANCELED);
        $order->setState(Order::STATE_CANCELED);

        $order->save();

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $url = $storeManager->getStore()->getUrl('customer/account/', array('_secure'=>true));

        $resultRedirect->setPath($url);

        return $resultRedirect;
    }
}