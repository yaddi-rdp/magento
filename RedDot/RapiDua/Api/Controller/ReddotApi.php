<?php
namespace Reddot\RapiDua\Api\Controller;

use Reddot\RapiDua\Api\CustomRepositoryInterface;
use Reddot\RapiDua\Helper\LogHelper;
use Magento\Framework\Webapi\Rest\Request;

class ReddotApi implements CustomRepositoryInterface
{ 

    protected $request;
    protected $dataHelper;
    protected $store;
    protected $resultRedirect;

    public function __construct(
        Request $request,
        \Reddot\RapiDua\Helper\Data $dataHelper,
        \Magento\Framework\Controller\ResultFactory $result
    ) {
        $this->request = $request;
        $this->dataHelper = $dataHelper;
        $this->resultRedirect = $result;
    }

    public function rapiCallback(){
        $responseData = json_decode(file_get_contents('php://input'), true);
        $signature = $responseData['signature'];

        $createResponseSignature = $this->dataHelper->createResponseSignature($responseData);

        if ($createResponseSignature != $responseData['signature']) {
            return [[
                'code' => 40,
                'message' => 'Invalid signature'
            ]];
        }

        if ($responseData['response_msg'] == 'successful' || $responseData['response_code'] == 0){
            $this->dataHelper->updateOrderStatus($responseData['order_id'], 'processing');
        } else {
            $this->dataHelper->updateOrderStatus($responseData['order_id'], 'canceled');
        }

        return [[
          'code' => 200,
          'message' => 'Success'
        ]];
    }

    public function rapiGetRedirectUrl(){
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $om->get('Magento\Customer\Model\Session');

        $redirectUrl = $session->getPaymentUrl();
        $orderId = $session->getOrderId();

        $session->setPaymentUrl(null);
        $session->setOrderId(null);

        $this->dataHelper->updateOrderStatus($orderId, 'pending_payment');

        if ($redirectUrl == null){
            return [[
              'code' => 40,
              'url' => '/'
            ]];
        }

        return [[
          'code' => 200,
          'url' => $redirectUrl
        ]];
    }
}