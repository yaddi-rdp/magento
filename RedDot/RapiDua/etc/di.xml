<?xml version='1.0'?>
<!--
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
-->

<config xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='urn:magento:framework:ObjectManager/etc/config.xsd'>
    <preference for='Reddot\RapiDua\Api\CustomRepositoryInterface' type='Reddot\RapiDua\Api\Controller\ReddotApi'/>

    <!-- Payment Method Facade configuration -->
    <virtualType name='RapiDuaFacade' type='Magento\Payment\Model\Method\Adapter'>
        <arguments>
            <argument name='code' xsi:type='const'>\Reddot\RapiDua\Model\Ui\ConfigProvider::CODE</argument>
            <argument name='formBlockType' xsi:type='string'>Magento\Payment\Block\Form</argument>
            <argument name='infoBlockType' xsi:type='string'>Reddot\RapiDua\Block\Info</argument>
            <argument name='valueHandlerPool' xsi:type='object'>RapiValueHandlerPool</argument>
            <argument name='commandPool' xsi:type='object'>RapiCommandPool</argument>
        </arguments>
    </virtualType>

    <!-- Configuration reader -->
    <virtualType name='RapiDuaConfig' type='Magento\Payment\Gateway\Config\Config'>
        <arguments>
            <argument name='methodCode' xsi:type='const'>\Reddot\RapiDua\Model\Ui\ConfigProvider::CODE</argument>
        </arguments>
    </virtualType>

    <!-- Logger, initialized with RapiConfig -->
    <virtualType name='RapiDuaLogger' type='Magento\Payment\Model\Method\Logger'>
        <arguments>
            <argument name='config' xsi:type='object'>RapiDuaConfig</argument>
        </arguments>
    </virtualType>

    <type name='Reddot\RapiDua\Gateway\Http\Client\ClientMock'>
        <arguments>
            <argument name='logger' xsi:type='object'>RapiDuaLogger</argument>
        </arguments>
    </type>

    <!-- Commands infrastructure -->
    <virtualType name='RapiDuaCommandPool' type='Magento\Payment\Gateway\Command\CommandPool'>
        <arguments>
            <argument name='commands' xsi:type='array'>
                <item name='authorize' xsi:type='string'>RapiDuaAuthorizeCommand</item>
                <item name='capture' xsi:type='string'>RapiDuaCaptureCommand</item>
                <item name='void' xsi:type='string'>RapiDuaVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>

    <!-- Authorize command -->
    <virtualType name='RapiDuaAuthorizeCommand' type='Magento\Payment\Gateway\Command\GatewayCommand'>
        <arguments>
            <argument name='requestBuilder' xsi:type='object'>RapiDuaAuthorizationRequest</argument>
            <argument name='handler' xsi:type='object'>RapiDuaResponseHandlerComposite</argument>
            <argument name='transferFactory' xsi:type='object'>Reddot\RapiDua\Gateway\Http\TransferFactory</argument>
            <argument name='client' xsi:type='object'>Reddot\RapiDua\Gateway\Http\Client\ClientMock</argument>
        </arguments>
    </virtualType>

    <!-- Authorization Request -->
    <virtualType name='RapiDuaAuthorizationRequest' type='Magento\Payment\Gateway\Request\BuilderComposite'>
        <arguments>
            <argument name='builders' xsi:type='array'>
                <item name='transaction' xsi:type='string'>Reddot\RapiDua\Gateway\Request\AuthorizationRequest</item>
                <item name='mockData' xsi:type='string'>Reddot\RapiDua\Gateway\Request\MockDataRequest</item>
            </argument>
        </arguments>
    </virtualType>
    <type name='Reddot\RapiDua\Gateway\Request\AuthorizationRequest'>
        <arguments>
            <argument name='config' xsi:type='object'>RapiDuaConfig</argument>
        </arguments>
    </type>

    <!-- Capture command -->
    <virtualType name='RapiDuaCaptureCommand' type='Magento\Payment\Gateway\Command\GatewayCommand'>
        <arguments>
            <argument name='requestBuilder' xsi:type='object'>Reddot\RapiDua\Gateway\Request\CaptureRequest</argument>
            <argument name='handler' xsi:type='object'>Reddot\RapiDua\Gateway\Response\TxnIdHandler</argument>
            <argument name='transferFactory' xsi:type='object'>Reddot\RapiDua\Gateway\Http\TransferFactory</argument>
            <argument name='validator' xsi:type='object'>Reddot\RapiDua\Gateway\Validator\ResponseCodeValidator</argument>
            <argument name='client' xsi:type='object'>Reddot\RapiDua\Gateway\Http\Client\ClientMock</argument>
        </arguments>
    </virtualType>

    <!-- Capture Request -->
    <type name='Reddot\RapiDua\Gateway\Request\CaptureRequest'>
        <arguments>
            <argument name='config' xsi:type='object'>RapiConfig</argument>
        </arguments>
    </type>

    <!-- Void command -->
    <virtualType name='RapiDuaVoidCommand' type='Magento\Payment\Gateway\Command\GatewayCommand'>
        <arguments>
            <argument name='requestBuilder' xsi:type='object'>Reddot\RapiDua\Gateway\Request\VoidRequest</argument>
            <argument name='handler' xsi:type='object'>Reddot\RapiDua\Gateway\Response\TxnIdHandler</argument>
            <argument name='transferFactory' xsi:type='object'>Reddot\RapiDua\Gateway\Http\TransferFactory</argument>
            <argument name='validator' xsi:type='object'>Reddot\RapiDua\Gateway\Validator\ResponseCodeValidator</argument>
            <argument name='client' xsi:type='object'>Reddot\RapiDua\Gateway\Http\Client\ClientMock</argument>
        </arguments>
    </virtualType>

    <!-- Void Request -->
    <type name='Reddot\RapiDua\Gateway\Request\VoidRequest'>
        <arguments>
            <argument name='config' xsi:type='object'>RapiConfig</argument>
        </arguments>
    </type>

    <!-- Response handlers -->
    <virtualType name='RapiDuaResponseHandlerComposite' type='Magento\Payment\Gateway\Response\HandlerChain'>
        <arguments>
            <argument name='handlers' xsi:type='array'>
                <item name='txnid' xsi:type='string'>Reddot\RapiDua\Gateway\Response\TxnIdHandler</item>
                <item name='fraud' xsi:type='string'>Reddot\RapiDua\Gateway\Response\FraudHandler</item>
            </argument>
        </arguments>
    </virtualType>

    <!-- Value handlers infrastructure -->
    <virtualType name='RapiDuaValueHandlerPool' type='Magento\Payment\Gateway\Config\ValueHandlerPool'>
        <arguments>
            <argument name='handlers' xsi:type='array'>
                <item name='default' xsi:type='string'>RapiDuaConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name='RapiDuaConfigValueHandler' type='Magento\Payment\Gateway\Config\ConfigValueHandler'>
        <arguments>
            <argument name='configInterface' xsi:type='object'>RapiDuaConfig</argument>
        </arguments>
    </virtualType>

    <type name='Reddot\RapiDua\Block\Info'>
        <arguments>
            <argument name='config' xsi:type='object'>RapiDuaConfig</argument>
        </arguments>
    </type>

</config>
