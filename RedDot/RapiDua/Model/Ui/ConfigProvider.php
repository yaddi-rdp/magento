<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Reddot\RapiDua\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Reddot\RapiDua\Gateway\Http\Client\ClientMock;
use Magento\Framework\View\Asset\Source;
use Reddot\RapiDua\Helper\LogHelper;
use Reddot\RapiDua\Helper\Config;
use Magento\Store\Model\StoreManagerInterface;
/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{

    private $tokenFactory;
    private $customerSession;
    private $template;
    private $adminConfig;
    protected $storeManager;

    /**
    * @param CcConfig $ccConfig
    * @param Source $assetSource
    */
    public function __construct(
        \Magento\Framework\View\Element\Template $template,
        Config $adminConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->template = $template;
        $this->adminConfig = $adminConfig;
        $this->storeManager = $storeManager;
    }
 
    /**
    * @var string[]
    */
    const CODE = 'reddot_rapidua';
    /**
    * {@inheritdoc}
    */
    public function getConfig()
    {
        $payment_logo = '';

        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        if ($this->adminConfig->getGeneralConfig('payment_logo') != ''){
            $payment_logo = $mediaUrl.'reddot/RapiDua/images/'.$this->adminConfig->getGeneralConfig('payment_logo');
        }

        return [
            'payment' => [
                'reddot_rapidua' => [
                    'paymentLogoUrl' => $payment_logo
                ]
            ]
        ];
    }
}