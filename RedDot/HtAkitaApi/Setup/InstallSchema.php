<?php

namespace Reddot\HtAkitaApi\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Reddot\HtAkitaApi\Helper\LogHelper;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('reddot_htakitaapi_token');
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'user_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'User_ID'
                )
                ->addColumn(
                    'number_holder',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Number_Holder'
                )
                ->addColumn(
                    'expired_date',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Expired_Date'
                )
                ->addColumn(
                    'payer_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Payer_ID'
                )
                ->addColumn(
                    'payer_name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Payer_Name'
                )
                ->addColumn(
                    'payer_email',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Payer_Email'
                )
                ->addColumn(
                    'token_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Token_ID'
                )
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $sql = 'Select * FROM sales_order_status WHERE status = "authorized"';
        $result = $setup->getConnection()->fetchAll($sql);

        $data = []; $order_state_data = [];
        if (count($result) == 0){
            $data[] = ['status' => 'authorized', 'label' => 'Authorized'];
            $order_state_data[] = ['status' => 'authorized', 'state' => 'authorized', 'is_default' => 1, 'visible_on_front' => 1];
        }

        if (count($data) > 0){
            $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);
        }

        if (count($order_state_data) > 0){
            $setup->getConnection()->insertArray($setup->getTable('sales_order_status_state'), ['status', 'state', 'is_default', 'visible_on_front'], $order_state_data);
        }

        $installer->endSetup();
    }
}