<?php

namespace Reddot\HtAkitaApi\Helper;

use Reddot\HtAkitaApi\Helper\LogHelper;

class Merchant
{
    public function callMerchantApi($method, $param, $url) {
        $chSign = curl_init();
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER =>  array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ),
        ];

        if ($method !== 'GET' && $param) {
            $options[CURLOPT_POSTFIELDS] = json_encode($param);
            $options[CURLOPT_CUSTOMREQUEST] = $method;
        }

        curl_setopt_array($chSign, $options);

        $res = curl_exec($chSign);
        $error = curl_error($chSign);

        if ($error) {
            return $error;
        }

        $resStatus = curl_getinfo($chSign, CURLINFO_HTTP_CODE);

        if ($resStatus < 200 || $resStatus >= 300) {
            return $res;
        }
        
        $resJson = json_decode($res);

        curl_close($chSign);
        return $resJson;
    }

    public function makeRequestGetRedirectionInfo($transactionId, $config){
        $requestData = [
            'request_mid' => $config->getGeneralConfig('merchant_id'),
            'transaction_id' => $transactionId
        ];

        $signatureData = ['signature' => $this->calculateSignature($requestData, $config->getGeneralConfig('secret_key'))];

        $requestData = array_merge($signatureData, $requestData);

        return $requestData;
    }

    private function calculateSignature($params, $secretKey){
        unset($params['signature']);
        ksort($params);
        $data_to_sign = '';

        foreach ($params as $v) {
            $data_to_sign .= $v;
        }
        $data_to_sign .= $secretKey;

        return hash('sha512', $data_to_sign);
    }
}