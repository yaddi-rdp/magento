<?php

namespace Reddot\HtAkitaApi\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Sales\Model\Order;

class Data extends AbstractHelper
{
    protected $store;
    protected $configHelper;
    protected $orderRepository;

    public function __construct(
        StoreManagerInterface $storeManager,
        \Reddot\HtAkitaApi\Helper\Config $configHelper,
        \Magento\Sales\Api\Data\OrderInterface $orderRepository
    ) {
        $this->store = $storeManager->getStore();
        $this->configHelper = $configHelper;
        $this->orderRepository = $orderRepository;
    }

    public function makePaymentRequestData($order, $payment){
        $requestData = [
            'mid' => $this->configHelper->getGeneralConfig('merchant_id'),
            'order_id' => (string) $order->getOrderIncrementId(),
            'ccy' => $this->configHelper->getGeneralConfig('currency'),
            'amount' => round($order->getGrandTotalAmount(), 2),
            'api_mode' => 'redirection_hosted',
            'payment_type' => (string) $this->configHelper->getGeneralConfig('payment_type'),
            'merchant_reference' => $payment->getAdditionalInformation('note'),
            'payer_id' => $payment->getAdditionalInformation('payerId'),
            'payer_email' => $payment->getAdditionalInformation('payerEmail'),
            'back_url' => $this->store->getBaseUrl().'reddot-payment/htapi/HostedBackAccountDetail?order_id='.$order->getOrderIncrementId(),
            'redirect_url' => $this->store->getBaseUrl().'reddot-payment/htapi/index',
            'notify_url' => $this->store->getBaseUrl(). 'index.php/rest/V1/htAkitaApiCallback',            
        ];

        if ($this->configHelper->getGeneralConfig('payment_type') == 'I') { 
            $requestData = array_merge($requestData, ['installment_tenor_month' => $this->configHelper->getGeneralConfig('installment_month')]);
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
         
        $billingAddress = $cart->getQuote()->getBillingAddress();
        $billingAddressData = $this->makeBillingAddress($billingAddress);

        $shippingAddress = $cart->getQuote()->getShippingAddress();
        $shippingAddressData = $this->makeShippingAddress($shippingAddress);

        $requestData = array_merge($requestData, $billingAddressData);
        $requestData = array_merge($requestData, $shippingAddressData);

        $signature = ['signature' => $this->createPaymentSignature($requestData)];
        $requestData = array_merge($requestData, $signature);

        return $requestData;
    }

    public function createPaymentSignature($signatureInfo){
        $secretKey = $this->configHelper->getGeneralConfig('secret_key');

        $fieldsForSign = array('mid', 'order_id', 'payment_type', 'amount', 'ccy');

        $aggregatedFieldStr = '';
        foreach ($fieldsForSign as $f) {
            $aggregatedFieldStr .= trim($signatureInfo[$f]);
        }

        $aggregatedFieldStr .= $signatureInfo['payer_id'];
        $aggregatedFieldStr .= $secretKey;

        $signature = hash('sha512', $aggregatedFieldStr);

        return $signature;
    }

    public function makeRequestDirectTokenData($orderId, $creditCardData){
        $requestData = [
            'mid' => $this->configHelper->getGeneralConfig('merchant_id'),
            'order_id' => (string) $orderId,
            'ccy' => $this->configHelper->getGeneralConfig('currency'),
            'transaction_type' => $creditCardData['transactionType'],
            'api_mode' => 'hosted_token_api',
            'payer_email' => $creditCardData['payerEmail'],
            'back_url' => $this->store->getUrl('checkout/', array('_secure'=>true)).'#payment',
            'redirect_url' => $this->store->getBaseUrl().'reddot-payment/htapi/hostedRedirect',
            'notify_url' => $this->store->getBaseUrl(). 'index.php/rest/V1/htAkitaApiHostedCallback',      
        ];

        if ($creditCardData['transactionType'] == 'R'){
            $requestData['api_mode'] = 'direct_token_api';
        }

        if ($creditCardData['transactionType'] == 'M' || $creditCardData['transactionType'] == 'R'){
            $requestData = array_merge($requestData, [ 'payer_id' => $creditCardData['payerId'] ]);    
        }

        $signature = ['signature' => $this->createSignature($requestData)];
        $requestData = array_merge($requestData, $signature);

        return $requestData;
    }

    public function callApi($method, $param, $payment) {
        if ($payment == 'token'){
            $url = 'https://secure-dev.reddotpayment.com/service/token-api';

            if ($this->configHelper->getGeneralConfig('environment') == 'production') {
                $url = 'https://secure.reddotpayment.com/service/token-api';
            }
        }else{
            $url = 'https://secure-dev.reddotpayment.com/service/payment-api';

            if ($this->configHelper->getGeneralConfig('environment') == 'production') {
                $url = 'https://secure.reddotpayment.com/service/payment-api';
            }
        }

        $chSign = curl_init();
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER =>  array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ),
        ];

        if ($method !== 'GET' && $param) {
            $options[CURLOPT_POSTFIELDS] = json_encode($param);
            $options[CURLOPT_CUSTOMREQUEST] = $method;
        }

        curl_setopt_array($chSign, $options);

        $res = curl_exec($chSign);
        $error = curl_error($chSign);

        if ($error) {
            return $error;
        }

        $resStatus = curl_getinfo($chSign, CURLINFO_HTTP_CODE);

        if ($resStatus < 200 || $resStatus >= 300) {
            return $res;
        }
        
        $resJson = json_decode($res);

        curl_close($chSign);
        return $resJson;
    }

    private function makeBillingAddress($billingAddress){
        $returnData = [];

        if (isset($billingAddress)){
            $returnData = [
                'bill_to_forename' => $billingAddress['firstname'],
                'bill_to_surname' => $billingAddress['lastname'],
                'bill_to_address_city' => $billingAddress['city'],
                'bill_to_address_line1' => $billingAddress['street'],
                'bill_to_address_line2' => $billingAddress['region'],
                'bill_to_address_country' => $billingAddress['region_code'],
                'bill_to_address_state' => $billingAddress['country_id'],
                'bill_to_address_postal_code' => $billingAddress['postcode'],
                'bill_to_phone' => $billingAddress['telephone']
            ];
        }

        return $returnData;
    }

    private function makeShippingAddress($shippingAddress){
        $returnData = [];

        if (isset($shippingAddress)){
            $returnData = [
                'ship_to_forename' => $shippingAddress['firstname'],
                'ship_to_surname' => $shippingAddress['lastname'],
                'ship_to_address_city' => $shippingAddress['city'],
                'ship_to_address_line1' => $shippingAddress['street'],
                'ship_to_address_line2' => $shippingAddress['region'],
                'ship_to_address_country' => $shippingAddress['region_code'],
                'ship_to_address_state' => $shippingAddress['country_id'],
                'ship_to_address_postal_code' => $shippingAddress['postcode'],
                'ship_to_phone' => $shippingAddress['telephone']
            ];
        }

        return $returnData;
    }

    public function updateOrderStatus($orderId, $status) {
        $order = $this->orderRepository->loadByIncrementId($orderId);

        if ($status == 'processing'){
            $order->setStatus(Order::STATE_PROCESSING);
            $order->setState(Order::STATE_PROCESSING);
        }else if($status == 'pending'){
            $order->setStatus('pending');
            $order->setState('pending');
        }else if($status == 'pending_payment'){
            $order->setStatus(Order::STATE_PENDING_PAYMENT);
            $order->setState(Order::STATE_PENDING_PAYMENT);
        }else if ($status == 'authorized'){
            $order->setStatus('authorized');
            $order->setState('authorized');
        }else {
            $order->setStatus(Order::STATE_CANCELED);
            $order->setState(Order::STATE_CANCELED);
        }

        $order->save();
    }

    public function createSignature($params){
        $secretKey = $this->configHelper->getGeneralConfig('secret_key');

        unset($params['signature']);
        ksort($params);
        $data_to_sign = '';

        foreach ($params as $v) {
            $data_to_sign .= $v;
        }
        $data_to_sign .= $secretKey;

        return hash('sha512', $data_to_sign);
    }

    public function createResponseSignature($response) {
        $secret_key = $this->configHelper->getGeneralConfig('secret_key');

        $response = json_decode(json_encode($response), true);
        unset($response['signature']);

        $data_to_sign = '';
        $this->recursiveGenericArraySign($response, $data_to_sign);
        $data_to_sign .= $secret_key;

        return hash('sha512', $data_to_sign);
    }

    private function recursiveGenericArraySign(&$params, &$data_to_sign){
        ksort($params);

        foreach ($params as $v) {
            if (is_array($v)) {
                recursiveGenericArraySign($v, $data_to_sign);
            } else {
                $data_to_sign .= $v;
            }
        }
    }
}