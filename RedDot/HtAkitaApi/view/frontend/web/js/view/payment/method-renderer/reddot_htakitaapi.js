define(
    [
        'underscore',
        'jquery',
        'Magento_Payment/js/view/payment/cc-form',
        'Magento_Payment/js/model/credit-card-validation/credit-card-data',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/translate',
        'mage/storage',
        'Reddot_HtAkitaApi/js/action/set-payment-method-action',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer'
    ],
    function (_, $, Component, creditCardData, additionalValidators, $t, storage, setPaymentMethodAction, quote, customer) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Reddot_HtAkitaApi/payment/form',
                note: '',
                creditCardId: '',
                redirectAfterPlaceOrder: false
            },
            initObservable: function() {
                this._super()
                    .observe([
                        'note',
                        'creditCardId'
                    ]);
                return this;
            },

            getCode: function() {
                return 'reddot_htakitaapi';
            },

            setValidateHandler: function(handler) {
                this.validateHandler = handler;
            },

            afterPlaceOrder: function () {
                setPaymentMethodAction(this.messageContainer);
                return false;
            },

            sendRequest: function(){
                var action = $('#htakitaapi-action-type').val();
                var transactionType = 'C'

                if (action == 'edit') {
                    transactionType = 'M'
                } else if (action == 'delete'){
                    transactionType = 'R'
                }

                var serviceUrl, payload

                var body = $('body').loader()
                body.loader('show')

                serviceUrl = 'index.php/rest/V1/htAkitaApiCreditCardAction';
                payload = {
                    transactionType: transactionType,
                    payerEmail: customer.customerData.email,
                    creditCardId: $('#htakitaapi-credit-card-id').val()
                };

                if (transactionType == 'R') {
                    var cardInfo = this.currentCreditCard()[$('#htakitaapi-credit-card-id').val()]

                    payload = {
                        transactionType: transactionType,
                        creditCardId: $('#htakitaapi-credit-card-id').val()
                    };
                }

                storage.post(
                    serviceUrl,
                    JSON.stringify(payload),
                    true
                ).done(function (response) {
                    var data = response[0]

                    if (data.code == 400){
                        body.loader('hide')
                        alert(data.message)
                    }else if(data.code == 200){
                        if (transactionType == 'R'){
                            location.reload();
                        }else{
                            $.mage.redirect(data.redirect_url);
                        }
                    }
                }).fail(function (response) {
                    body.loader('hide')
                    alert('Server Error')
                })
            },

            getData: function () {
                var data = {
                    'method': this.item.method,
                    'additional_data': {
                        'creditCardData': JSON.stringify(creditCardData),
                        'creditCardId': $('#htakitaapi-credit-card-id').val(),
                        'note': this.note()
                    }
                };

                return data;
            },

            isActive: function() {
                return true;
            },

            getImageUrl: function() {
                return window.checkoutConfig.payment.reddot_htakitaapi.imageUrl;
            },

            getPaymentLogoUrl: function() {
                return window.checkoutConfig.payment.reddot_htakitaapi.paymentLogoUrl;
            },

            currentCreditCard: function() {
                return window.checkoutConfig.payment.reddot_htakitaapi.currentCreditCard;
            },

            showEditButton: function(){
                if (this.currentCreditCard().length < 1){
                    return false;
                }else{
                    return true;
                }
            },

            setPlaceOrderHandler: function(handler) {
                this.placeOrderHandler = handler;
            },

            setValidateHandler: function(handler) {
                this.validateHandler = handler;
            },

            context: function() {
                return this;
            },

            getCurrentCreditCards: function() {
                return _.map(this.currentCreditCard(), function(value, key) {
                    return {
                        'value': key,
                        'data': value
                    }
                });
            },

            validate: function () {
                if ($('#htakitaapi-credit-card-id').val() == '') {
                    alert('Please select Card');
                    return false;
                }
                
                return true;
            },

            addNewCreditCard: function(e) {
                $('#htakitaapi-action-type').val('add');
                this.sendRequest();
            },

            editCreditCard: function(e) {
                if ($('#htakitaapi-credit-card-id').val() == ''){
                    return alert('Please select Card')
                }

                var cardInfo = this.currentCreditCard()[$('#htakitaapi-credit-card-id').val()]

                $('#htakitaapi-action-type').val('edit');

                this.sendRequest();
            },

            deleteCreditCard: function(e) {
                if ($('#htakitaapi-credit-card-id').val() == ''){
                    return alert('Please select Card') 
                }

                $('#htakitaapi-action-type').val('delete');

                this.sendRequest();
            }
        });
    }
);

function htakitaapiSetCardID(value){
    document.getElementById('htakitaapi-credit-card-id').value = value
    document.getElementById('htakitaapi-place-group').style.display = 'block'
}