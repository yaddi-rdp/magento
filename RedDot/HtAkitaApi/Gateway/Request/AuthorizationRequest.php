<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Reddot\HtAkitaApi\Gateway\Request;

use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Reddot\HtAkitaApi\Helper\LogHelper;
use Reddot\HtAkitaApi\Helper\Data;
use Reddot\HtAkitaApi\Helper\Config;


class AuthorizationRequest implements BuilderInterface
{
    /**
     * @var ConfigInterface
     */
    private $config;
    private $dataHelper;
    private $configHelper;
    private $tokenFactory;

    /**
     * @param ConfigInterface $config
     */
    public function __construct(
        Data $dataHelper,
        \Reddot\HtAkitaApi\Helper\Config $configHelper,
        ConfigInterface $config,
        \Reddot\HtAkitaApi\Model\TokenFactory $tokenFactoryModel
    ) {
        $this->config = $config;
        $this->dataHelper = $dataHelper;
        $this->configHelper = $configHelper;
        $this->tokenFactory = $tokenFactoryModel;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject)
    {
        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        $paymentDO = $buildSubject['payment'];
        $order = $paymentDO->getOrder();
        $payment = $paymentDO->getPayment();

        $object = $this->tokenFactory->create();
        $object->load($payment->getAdditionalInformation('creditCardId'));

        /** @var PaymentDataObjectInterface $payment */
        $payment->setAdditionalInformation('payerId', $object->getPayerId());
        $payment->setAdditionalInformation('payerEmail', $object->getPayerEmail());

        $requestParams = $this->dataHelper->makePaymentRequestData($order, $payment);

        $response = $this->dataHelper->callApi('POST', $requestParams, 'payment');

        if (!isset($response->response_code)){
            throw new CouldNotSaveException(__($response));
        }

        if ($response->response_code != '0') {
            throw new CouldNotSaveException(__($response->response_msg));
        }

        $createResponseSignature = $this->dataHelper->createResponseSignature($response);

        if ($response->signature != $createResponseSignature) {
            throw new CouldNotSaveException(__('Invalid signature'));
        }

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $om->get('Magento\Customer\Model\Session');
        $session->setPaymentUrl($response->payment_url);
        $session->setOrderId($order->getOrderIncrementId());

        return [
            'TXN_TYPE' => 'A',
            'INVOICE' => $response->transaction_id,
            'AMOUNT' => $order->getGrandTotalAmount(),
            'CURRENCY' => $order->getCurrencyCode(),
            'EMAIL' => '',
            'MERCHANT_KEY' => $this->config->getValue(
                'merchant_gateway_key',
                $order->getStoreId()
            )
        ];
    }
}
