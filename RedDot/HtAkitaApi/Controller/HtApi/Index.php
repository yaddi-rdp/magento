<?php 
namespace Reddot\HtAkitaApi\Controller\HtApi; 
 
use Magento\Framework\Controller\ResultFactory;
use Reddot\HtAkitaApi\Helper\LogHelper;
use Reddot\HtAkitaApi\Helper\Merchant;
use Magento\Sales\Model\Order;

class Index extends \Magento\Framework\App\Action\Action
{ 

    protected $config;
    protected $orderRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Reddot\HtAkitaApi\Helper\Config $config,
        \Magento\Sales\Api\Data\OrderInterface $orderRepository
    ) {
        $this->config = $config;
        $this->orderRepository = $orderRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $transaction_id = $_GET['transaction_id'];

        $url = 'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_redirection';

        if ($this->config->getGeneralConfig('environment') == 'production') {
            $url = 'https://secure.reddotpayment.com/service/Merchant_processor/query_redirection';
        }

        $request_params = (new Merchant())->makeRequestGetRedirectionInfo($transaction_id, $this->config);
        $response = (new Merchant())->callMerchantApi('POST', $request_params, $url);
        $order = $this->orderRepository->loadByIncrementId($response->order_id);

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ((int)$response->response_code == 0 || $response->response_msg == 'successful'){
            if ($order->getStatus() == 'pending_payment'){
                if ($response->transaction_type == 'A'){
                    $order->setStatus('authorized');
                    $order->setState('authorized');
                }else if($response->transaction_type == 'S'){
                    $order->setStatus(Order::STATE_PROCESSING);
                    $order->setState(Order::STATE_PROCESSING);
                }

                $order->save();
            }

            $resultRedirect->setPath('checkout/onepage/success');
        }else{
            $resultRedirect->setPath('checkout/onepage/failure');
        }

        return $resultRedirect;
    }
}