<?php 
namespace Reddot\HtAkitaApi\Controller\HtApi; 
 
use Magento\Framework\Controller\ResultFactory;
use Reddot\HtAkitaApi\Helper\LogHelper;
use Reddot\HtAkitaApi\Helper\Merchant;

class HostedRedirect extends \Magento\Framework\App\Action\Action
{ 

    protected $config;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Reddot\HtAkitaApi\Helper\Config $config
    ) {
        $this->config = $config;
        parent::__construct($context);
    }

    public function execute()
    {
        $transaction_id = $_GET['transaction_id'];

        $url = 'http://secure-dev.reddotpayment.com/service/Merchant_processor/query_token_redirection';

        if ($this->config->getGeneralConfig('environment') == 'production') {
            $url = 'https://secure.reddotpayment.com/service/Merchant_processor/query_token_redirection';
        }

        $request_params = (new Merchant())->makeRequestGetRedirectionInfo($transaction_id, $this->config);
        $response = (new Merchant())->callMerchantApi('POST', $request_params, $url);

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $tokenFactoryModel = $this->_objectManager->create('Reddot\HtAkitaApi\Model\TokenFactory');
        $customerSession = $this->_objectManager->create('Magento\Customer\Model\Session');
        $storeManager = $this->_objectManager->create('Magento\Store\Model\StoreManagerInterface');

        if ((int)$response->response_code == 0 || $response->response_msg == 'successful'){
            if ($response->transaction_type == 'C' || $response->transaction_type == 'M'){
                $objects = $tokenFactoryModel->create()->getCollection()
                    ->addFieldToFilter('payer_id', array('eq' => $response->payer_id));

                if (count($objects) > 0){
                    foreach ($objects as $item ){
                        $object = $tokenFactoryModel->create();
                        $object->load((int) $item->getID());
                        $object->setData('payer_id', $response->payer_id);
                        if (isset($response->payer_email)){
                            $object->setData('payer_email', $response->payer_email);    
                        }

                        if (isset($response->last_4)){
                            $object->setData('number_holder', $response->last_4);    
                        }

                        if (isset($response->exp_date)){
                            $object->setData('expired_date', $response->exp_date);    
                        }

                        if (isset($response->payer_name)){
                            $object->setData('payer_name', $response->payer_name);    
                        }

                        $object->setData('user_id', $customerSession->getCustomer()->getId());
                        $object->setData('token_id', $response->token_id);
                        $object->save();
                    }
                }else{
                    $tokenFactoryModel->create()->setData(
                        array(
                            'user_id' => $customerSession->getCustomer()->getId(),
                            'number_holder' => isset($response->last_4) ? $response->last_4 : '',
                            'expired_date' => isset($response->exp_date) ? $response->exp_date : '',
                            'payer_id' => $response->payer_id,
                            'payer_name' => isset($response->payer_name) ? $response->payer_name : '',
                            'payer_email' => isset($response->payer_email) ? $response->payer_email : '',
                            'token_id' => $response->token_id
                        )
                    )->save();
                }
            }
        }

        $url = $storeManager->getStore()->getUrl('checkout/', array('_secure'=>true)).'#payment';

        $resultRedirect->setPath($url);

        return $resultRedirect;
    }
}