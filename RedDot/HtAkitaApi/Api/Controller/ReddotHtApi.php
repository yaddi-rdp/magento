<?php
namespace Reddot\HtAkitaApi\Api\Controller;

use Reddot\HtAkitaApi\Api\CustomRepositoryInterface;
use Reddot\HtAkitaApi\Helper\LogHelper;
use Reddot\HtAkitaApi\Helper\Data;
use Magento\Framework\Webapi\Rest\Request;

class ReddotHtApi implements CustomRepositoryInterface
{ 

    protected $request;
    protected $dataHelper;
    protected $store;
    private $tokenFactory;
    private $customerSession;

    public function __construct(
        Request $request,
        \Magento\Store\Api\Data\StoreInterface $storeManager,
        Data $dataHelper,
        \Reddot\HtAkitaApi\Model\TokenFactory $tokenFactoryModel,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->request = $request;
        $this->dataHelper = $dataHelper;
        $this->store = $storeManager->getStore();
        $this->tokenFactory = $tokenFactoryModel;
        $this->customerSession = $customerSession;
    }

    public function htApiCreditCardAction(){
        $requestData = file_get_contents('php://input');
        $requestData = json_decode($requestData, true);

        if($requestData['transactionType'] == 'M' || $requestData['transactionType'] == 'R'){
            $object = $this->tokenFactory->create();
            $object->load((int) $requestData['creditCardId']);

            $requestData = array_merge($requestData, [ 'payerId' => $object->getPayerId()]);
            $requestData = array_merge($requestData, [ 'payerEmail' => $object->getPayerEmail()]);
        }

        $requestParams = $this->dataHelper->makeRequestDirectTokenData(date("Ymdhis"), $requestData);

        $response = $this->dataHelper->callApi('POST', $requestParams, 'token');

        if ((int) $response->response_code != 0){
            return [[
                'code' => 400,
                'message' => $response->response_msg
            ]];
        }
        
        $responseSignature = $this->dataHelper->createResponseSignature($response);

        if ($responseSignature != $response->signature){
            return [[
                'code' => 400,
                'message' => 'Invalid Signature'
            ]];   
        }

        if($requestParams['transaction_type'] == 'R'){
            $object = $this->tokenFactory->create();
            $object->load((int) $requestData['creditCardId']);
            $object->delete();
        }

        return [[
            'code' => 200,
            'message' => 'Success',
            'redirect_url' => isset($response->payment_url) ? $response->payment_url : ''
        ]];
    }

    public function htApiHostedCallback(){
        $responseData = json_decode(file_get_contents('php://input'), true);
        $signature = $responseData['signature'];

        $createResponseSignature = $this->dataHelper->createResponseSignature($responseData);

        if ($createResponseSignature != $responseData['signature']) {
            return [[
                'code' => 40,
                'message' => 'Invalid signature'
            ]];
        }

        if ($responseData['response_msg'] == 'successful' || $responseData['response_code'] == 0){
            if ($responseData['transaction_type'] == 'C' || $responseData['transaction_type'] == 'M'){
                $objects = $this->tokenFactory->create()->getCollection()
                    ->addFieldToFilter('payer_id', array('eq' => $responseData['payer_id']));

                if (count($objects) > 0){
                    foreach ($objects as $item ){
                        $object = $this->tokenFactory->create();
                        $object->load((int) $item->getID());
                        $object->setData('payer_id', $responseData['payer_id']);
                        if (isset($responseData['payer_email'])){
                            $object->setData('payer_email', $responseData['payer_email']);    
                        }

                        if (isset($responseData['last_4'])){
                            $object->setData('number_holder', $responseData['last_4']);    
                        }

                        if (isset($responseData['exp_date'])){
                            $object->setData('expired_date', $responseData['exp_date']);    
                        }

                        if (isset($responseData['payer_name'])){
                            $object->setData('payer_name', $responseData['payer_name']);    
                        }

                        $object->setData('token_id', $responseData['token_id']);
                        $object->save();
                    }
                }else{
                    $this->tokenFactory->create()->setData(
                        array(
                            'user_id' => $this->customerSession->getCustomer()->getId(),
                            'number_holder' => isset($responseData['last_4']) ? $responseData['last_4'] : '',
                            'expired_date' => isset($responseData['exp_date']) ? $responseData['exp_date'] : '',
                            'payer_id' => $responseData['payer_id'],
                            'payer_name' => isset($responseData['payer_name']) ? $responseData['payer_name'] : '',
                            'payer_email' => isset($responseData['payer_email']) ? $responseData['payer_email'] : '',
                            'token_id' => $responseData['token_id']
                        )
                    )->save();
                }
            }
        }

        return [[
            'code' => 200,
            'message' => 'Success'
        ]];
    }

    public function htApiCallback(){
        $responseData = json_decode(file_get_contents('php://input'), true);
        $signature = $responseData['signature'];

        $createResponseSignature = $this->dataHelper->createResponseSignature($responseData);

        if ($createResponseSignature != $responseData['signature']) {
            return [[
                'code' => 40,
                'message' => 'Invalid signature'
            ]];
        }

        if ($responseData['response_msg'] == 'successful' || $responseData['response_code'] == 0){
            if ($responseData['transaction_type'] == 'A'){
                $this->dataHelper->updateOrderStatus($responseData['order_id'], 'authorized');
            }else{
                $this->dataHelper->updateOrderStatus($responseData['order_id'], 'processing');
            }
        } else {
            $this->dataHelper->updateOrderStatus($responseData['order_id'], 'canceled');
        }

        return [[
            'code' => 200,
            'message' => 'Success'
        ]];
    }

    public function htApiGetRedirectUrl(){
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $om->get('Magento\Customer\Model\Session');

        $redirectUrl = $session->getPaymentUrl();
        $orderId = $session->getOrderId();

        $session->setPaymentUrl(null);
        $session->setOrderId(null);

        $this->dataHelper->updateOrderStatus($orderId, 'pending_payment');

        if ($redirectUrl == null){
            return [[
              'code' => 40,
              'url' => ''
            ]];
        }

        return [[
          'code' => 200,
          'url' => $redirectUrl
        ]];
    }
}