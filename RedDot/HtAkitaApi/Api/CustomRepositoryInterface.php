<?php

namespace Reddot\HtAkitaApi\Api;

interface CustomRepositoryInterface
{
  /**
   * Create custom Api.
   *
   * @return string
   * @throws \Magento\Framework\Exception\LocalizedException
   */
  public function htApiCreditCardAction();

  /**
   * Create custom Api.
   *
   * @return string
   * @throws \Magento\Framework\Exception\LocalizedException
   */
  public function htApiCallback();

  /**
   * Create custom Api.
   *
   * @return string
   * @throws \Magento\Framework\Exception\LocalizedException
   */
  public function htApiHostedCallback();

  /**
   * Create custom Api.
   *
   * @return string
   * @throws \Magento\Framework\Exception\LocalizedException
   */
  public function htApiGetRedirectUrl();
}