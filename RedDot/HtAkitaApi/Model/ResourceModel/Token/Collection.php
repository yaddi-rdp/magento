<?php
namespace Reddot\HtAkitaApi\Model\ResourceModel\Token;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {
    protected function _construct() {
        $this->_init(
            'Reddot\HtAkitaApi\Model\Token',
            'Reddot\HtAkitaApi\Model\ResourceModel\Token'
        );
    }
}