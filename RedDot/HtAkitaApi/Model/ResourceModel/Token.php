<?php
namespace Reddot\HtAkitaApi\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Token extends AbstractDb {
    protected function _construct(){
    	$this->_init('reddot_htakitaapi_token', 'id');
    }
}