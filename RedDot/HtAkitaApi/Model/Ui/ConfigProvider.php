<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Reddot\HtAkitaApi\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Reddot\HtAkitaApi\Gateway\Http\Client\ClientMock;
use Magento\Framework\View\Asset\Source;
use Reddot\HtAkitaApi\Helper\LogHelper;
use Reddot\HtAkitaApi\Helper\Config;
use Magento\Store\Model\StoreManagerInterface;
/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{

    private $tokenFactory;
    private $customerSession;
    private $template;
    private $adminConfig;
    protected $storeManager;

    /**
    * @param CcConfig $ccConfig
    * @param Source $assetSource
    */
    public function __construct(
        \Magento\Payment\Model\CcConfig $ccConfig,
        \Reddot\HtAkitaApi\Model\TokenFactory $tokenFactoryModel,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\View\Element\Template $template,
        Source $assetSource,
        Config $adminConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->ccConfig = $ccConfig;
        $this->assetSource = $assetSource;
        $this->tokenFactory = $tokenFactoryModel;
        $this->customerSession = $customerSession;
        $this->template = $template;
        $this->adminConfig = $adminConfig;
        $this->storeManager = $storeManager;
    }
 
    /**
    * @var string[]
    */
    const CODE = 'reddot_htakitaapi';
    /**
    * {@inheritdoc}
    */
    public function getConfig()
    {
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $payment_logo = '';

        if ($this->adminConfig->getGeneralConfig('payment_logo') != ''){
            $payment_logo = $mediaUrl.'reddot/HtAkitaApi/images/'.$this->adminConfig->getGeneralConfig('payment_logo');
        }

        return [
            'payment' => [
                'reddot_htakitaapi' => [
                    'currentCreditCard' => $this->getUserCreditCard(),
                    'paymentLogoUrl' => $payment_logo
                ]
            ]
        ];
    }

    private function getUserCreditCard(){
        $currentCreditCard = [];

        $userId = $this->customerSession->getCustomer()->getId();

        $objects = $this->tokenFactory->create()->getCollection()
            ->addFieldToFilter('user_id', array('eq' => $userId));

        foreach ($objects as $object ){
            $currentCreditCard[(string) $object->getID()] = [ 
                'id' => $object->getID(),
                'number' => $object->getNumberHolder() != '' ? '**** **** **** '.$object->getNumberHolder() : '',
                'validNumber' => (int) $object->getNumberHolder(),
                'email' => $object->getPayerEmail(),
                'name' => $object->getPayerName(),
                'payerId' => mb_substr($object->getPayerId(), 0, 12).'****'
            ];
        }

        return $currentCreditCard;
    }
}