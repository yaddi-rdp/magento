<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Reddot\HtAkitaApi\Model\Adminhtml\Source;

use Magento\Payment\Model\Method\AbstractMethod;

/**
 * Class PaymentAction
 */
class MerchantAction implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'capture',
                'label' => 'Capture'
            ],
            [
                'value' => 'void',
                'label' => 'Void'
            ],
            [
                'value' => 'refund',
                'label' => 'Refund'
            ]
        ];
    }
}
