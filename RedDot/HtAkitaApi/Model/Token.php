<?php
namespace Reddot\HtAkitaApi\Model;

use Magento\Framework\Model\AbstractModel;

class Token extends AbstractModel {
    protected function _construct() {
    	$this->_init('Reddot\HtAkitaApi\Model\ResourceModel\Token');
    }
}